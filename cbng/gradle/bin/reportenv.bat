@rem echo off
rem Reports the current development environment settings
echo.
echo ================================================
echo  Java Runtime Environment Report
echo ------------------------------------------------
echo JAVA_HOME=%JAVA_HOME%
echo Java executable: %JAVA_EXE% with version...
"%JAVA_EXE%" -version 2>&1
echo.
echo CLASSPATH=%CLASSPATH%
echo DEFAULT_JVM_OPTS=%DEFAULT_JVM_OPTS%
echo JAVA_OPTS=%JAVA_OPTS%
echo.

set user_deployment_properties="%USERPROFILE%\.java\deployment\deployment.properties"
if not exist "%user_deployment_properties%" set prefix=No 
echo INFO: %prefix%%user_deployment_properties% file exists.

set system_deployment_properties="/etc/.java/deployment/deployment.config"
if exist "%system_deployment_properties%" echo WARNING: %system_deployment_properties% file exists.

set user_java_policy="%USERPROFILE%\.java.policy"
if exist "%user_java_policy%" echo INFO: %user_java_policy% exists (JVM system wide policy may be kludged too).

echo.
echo ------------------------------------------------
echo  Java Tools Report
echo ------------------------------------------------
if defined M2_HOME echo WARNING: M2_HOME is set to '%M2_HOME%'. This often conflicts with devtools' own maven settings.
echo.
echo GRADLE_HOME="%GRADLE_HOME%"
echo GRADLE_OPTS=%GRADLE_OPTS%
echo GRADLE_PARAMETERS=%GRADLE_PARAMETERS%
echo BOB_USER_CACHE=%BOB_USER_CACHE%
echo.
echo ================================================
echo.
