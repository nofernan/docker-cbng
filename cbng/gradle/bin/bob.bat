@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  Gradle startup script for Windows
@rem
@rem ##########################################################################
@rem This script is based on the Gradle startup script (gradle.bat)
@rem When editing this script, please make sure that the script remains easily diffable with gradle.bat

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

@rem Detect if executed on build server
if defined bamboo_BUILD_SERVER set BUILD_SERVER=%bamboo_BUILD_SERVER%

@rem Add default JVM options here. You can also use JAVA_OPTS and GRADLE_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.\
set APP_BASE_NAME=%~n0
if not defined GRADLE_HOME set GRADLE_HOME=%DIRNAME%..

@rem Set up default Gradle parameters
if defined BUILD_SERVER set GRADLE_PARAMETERS="%GRADLE_PARAMETERS% -Psvntag=false"

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

echo.
echo WARNING: JAVA_HOME is not set. Looking for global JAVA installation
echo.

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windowz variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%$

:execute
@rem If no option is set, print fast help
if [%1]==[] goto :printUsageAndExit

@rem Fast help if -?, -h or --help set, Gradle help if --gradle-help is set
:argloop
if not "%1"=="" (
    @rem If -?, -h or --help is set, then output help message and do not call Gradle
    if "%1"=="-?" goto :printUsageAndExit
    if "%1"=="-h" goto :printUsageAndExit
    if "%1"=="--help" goto :printUsageAndExit
    
    @rem If --gradle-help is set, display Gradle's help
    if "%1"=="--gradle-help" set CMD_LINE_ARGS=--help
    
    shift
    goto :argloop
)

@rem Setup the command line

@rem No recognition of build server here. Review if necessary.
if not defined BOB_USER_CACHE set BOB_USER_CACHE=c:\tmp\bob_cache_%USERNAME%

for %%i in (%GRADLE_HOME%\lib\gradle-launcher-*.jar) do set CLASSPATH=%%i

call "%GRADLE_HOME%/bin/reportenv.bat"

@rem Execute Gradle
"%JAVA_EXE%" ^
    %DEFAULT_JVM_OPTS% ^
    %JAVA_OPTS% ^
    %GRADLE_OPTS% ^
    "-Dorg.gradle.appname=%APP_BASE_NAME%" ^
    -Dorg.gradle.daemon=false ^
    -classpath "%CLASSPATH%" ^
    org.gradle.launcher.GradleMain ^
    --gradle-user-home %BOB_USER_CACHE% ^
    %GRADLE_PARAMETERS% ^
    %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable GRADLE_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%GRADLE_EXIT_CONSOLE%" exit 1
exit /b 1

:printUsageAndExit
    type "%~dp0\\help.txt"
exit /b 0

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
